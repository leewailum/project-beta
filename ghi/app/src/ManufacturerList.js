import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";


function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1 className="mt-4">Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map((manufacturer, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to="/manufacturers/create">
                <button type="button" className="btn btn-primary">
                    Add new manufacturer
                </button>
            </Link>
        </>
    );
}

export default ManufacturerList;
