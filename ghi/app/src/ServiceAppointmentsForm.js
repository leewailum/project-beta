import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";


function ServiceAppointmentsForm() {
    const navigate = useNavigate()

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const [formData, setFormData] = useState({
        date: "",
        time: "",
        reason: "",
        vin: "",
        customer: "",
        technician: "",
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const dateTime = `${formData.date}T${formData.time}:00.000Z`

        const formattedFormData = {
            date_time: dateTime,
            reason: formData.reason,
            vin: formData.vin,
            customer: formData.customer,
            technician: formData.technician,
        }
        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formattedFormData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                date: "",
                time: "",
                reason: "",
                vin: "",
                customer: "",
                technician: "",
            })
            navigate("/appointments")
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-service-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.customer} onChange={handleFormChange} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.date} onChange={handleFormChange} placeholder="date" required type="date" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.time} onChange={handleFormChange} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.technician} onChange={handleFormChange} required id="technician" name="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.reason} onChange={handleFormChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ServiceAppointmentsForm;
