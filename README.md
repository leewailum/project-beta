# CarCar

Team:

* Greg Herren - Service microservice
* Huilin Li - Sales microservice

## Design

## Service microservice

The shoes microservice has three models:
- Technician
- AutomobileVO
- Appointment

The Technician model has three fields: first_name, last_name, and employee_id.

The AutomobileVO model is a value object. There is a poller that runs every 60 seconds hitting the automobiles route of the inventory-api to ensure that the automobiles value-objects accurately reflect the dealership's inventory.

The Appointment model has six fields: date_time, reason, status, vin, customer, and isVip. Additionally, it has a foreign key to the technician model. By default, results are ordered in ascending order by appointment date_time.

## Sales microservice

models: Salesperson, Customer, Sale, AutomobileVO

AutomobileVO is value object created base on inventory microservice Automobile model, it gets Automobile data that i need through polling. Then sale get automobile from AutomobileVO through foreignkey key. So that Sales microservice get automobile data from inventory
microservice.
