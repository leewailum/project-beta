from django.urls import path

from .views import api_technicians, api_delete_technician, api_appointments, api_delete_appointment, api_update_appointment

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>", api_delete_technician, name="api_delete_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>", api_delete_appointment, name="api_delete_appointment"),
    path("appointments/<int:id>/<str:status>", api_update_appointment, name="api_update_appointment"),
]
